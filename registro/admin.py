from django.contrib import admin
from .models import *

class ActividadAdmin(admin.ModelAdmin):
    list_display = ('id','proyecto','persona_organiza','nombre_actividad','fecha')
    list_filter = ('persona_organiza','municipio',)
    search_fields = ('nombre_actividad',)

# Register your models here.
admin.site.register(Proyecto)
admin.site.register(Resultado)
admin.site.register(Organizador)
admin.site.register(Actividad, ActividadAdmin)
