# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from lugar.models import Municipio, Comunidad
from smart_selects.db_fields import ChainedForeignKey
import datetime

SI_NO = ((1, u'Si'), (2, u'No'))

class Proyecto(models.Model):
    nombre = models.CharField(max_length=250)
    codigo = models.CharField(max_length=20)
    inicio = models.DateField()
    finalizacion = models.DateField()
    monto_asignado = models.IntegerField()
    contacto = models.CharField(max_length=100, verbose_name='Persona de contacto')
    municipios = models.ManyToManyField(Municipio)

    def __unicode__(self):
        return u'%s-%s' % (self.nombre, self.codigo)

    class Meta:
        verbose_name_plural = u'Proyectos'

class Resultado(models.Model):
    nombre_corto = models.CharField(max_length=50)
    descripcion = models.TextField()
    proyecto = models.ForeignKey(Proyecto)

    def __unicode__(self):
        return u'%s' % self.nombre_corto

    class Meta:
        verbose_name = u'Resultado del proyecto'
        verbose_name_plural = u'Resultados del proyecto'

class Organizador(models.Model):
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return u'%s' % self.nombre

    class Meta:
        verbose_name_plural = u'Organizadores'

TIPO_ACTIVIDAD = ((1, u'Encuentro'), (2, u'Taller'),
                  (3, u'Cabildo'), (4, u'Reunión Comunitaria'),
                  (5, u'Campamento'), (6, u'Festival'),
                  (7, u'Feria'), (8, u'Gestión ante autoridades'),
                  (9, u'Diplomado'))

TEMA_ACTIVIDAD = ((1, u'Derechos Humanos'), (2, u'Empoderamiento'),
                  (3, u'Participación Cuidadana'), (4, u'Incidencia'),
                  (5, u'Marco Jurídico Nacional'), (6, u'Género'),
                  (7, u'Fortalecimiento de capacidades de la organización'))

EJES = ((1, u'Interculturalidad'), (2, u'Género'),
        (3, u'Medio ambiente'), (4, u'Generacional'))

EVALUACION = ((99, u'No aplica'), (1, u'Muy bueno'), (2, u'Bueno'),
              (3, u'Regular'), (4, u'Malo'), (5, u'Muy malo'))

class Actividad(models.Model):
    proyecto = models.ForeignKey(Proyecto)
    persona_organiza = models.ForeignKey(Organizador, verbose_name=u'Persona que organiza la actividad')
    nombre_actividad = models.CharField(max_length=150)
    fecha = models.DateTimeField()
    municipio = models.ForeignKey(Municipio)
    comunidad = models.ForeignKey(Comunidad)
    tipo_actividad = models.IntegerField(choices=TIPO_ACTIVIDAD)
    tema_actividad = models.IntegerField(choices=TEMA_ACTIVIDAD)
    ejes_transversales = models.IntegerField(choices=EJES)
    #participantes por sexo
    hombres = models.IntegerField(default=0)
    mujeres = models.IntegerField(default=0)
    #participantes por edad
    no_dato = models.BooleanField(verbose_name='No hay datos')
    adultos = models.IntegerField(default=0, verbose_name=u'Adultos/as')
    jovenes = models.IntegerField(default=0, verbose_name=u'Jóvenes')
    ninos = models.IntegerField(default=0, verbose_name=u'Niños/as')
    #participantes por tipo
    no_dato1 = models.BooleanField(verbose_name='No hay datos')
    autoridades = models.IntegerField(default=0, verbose_name=u'Autoridades públicas')
    maestros = models.IntegerField(default=0)
    lideres = models.IntegerField(default=0, verbose_name=u'Lideres/zas Comunitarios')
    pobladores = models.IntegerField(default=0, verbose_name=u'Pobladores/as')
    estudiantes = models.IntegerField(default=0)
    miembros = models.IntegerField(default=0, verbose_name=u'Miembros de organizaciones comunitarias')
    tecnicos = models.IntegerField(default=0, verbose_name=u'Técnicas/os de Organizaciones Copartes de TROCAIRE')
    resultado = models.ForeignKey(Resultado,
                                  verbose_name=u'Resultado al que aporta')
    #evaluaciones de hombres
    relevancia = models.IntegerField(choices=EVALUACION, verbose_name=u'Importancia del tema/acción')
    efectividad = models.IntegerField(choices=EVALUACION, verbose_name='Efectividad de la acción')
    aprendizaje = models.IntegerField(choices=EVALUACION, verbose_name=u'Grado de aprendizaje')
    empoderamiento = models.IntegerField(choices=EVALUACION, verbose_name=u'Nivel de apropiación')
    participacion = models.IntegerField(choices=EVALUACION, verbose_name=u'Nivel de participación')

    #evaluaciones de mujeres
    relevancia_m = models.IntegerField(choices=EVALUACION, verbose_name=u'Importancia del tema/acción')
    efectividad_m = models.IntegerField(choices=EVALUACION, verbose_name='Efectividad de la acción')
    aprendizaje_m = models.IntegerField(choices=EVALUACION, verbose_name=u'Grado de aprendizaje')
    empoderamiento_m = models.IntegerField(choices=EVALUACION, verbose_name=u'Nivel de apropiación')
    participacion_m = models.IntegerField(choices=EVALUACION, verbose_name=u'Nivel de participación')

    #recursos
    comentarios = models.TextField(blank=True, default='')
    acuerdos = models.TextField(blank=True, default='')
    #foto1 = models.ImageField(upload_to='fotos', blank=True, null=True)
    foto1 =  models.ImageField(upload_to='/fotos/', blank=True, null=True)
    foto2 = models.ImageField(upload_to='/fotos/', blank=True, null=True)
    foto3 = models.ImageField(upload_to='/fotos/', blank=True, null=True)
    video = models.CharField(max_length=300, blank=True, default='')

    mes = models.IntegerField(editable=False)
    year = models.IntegerField(editable=False)

    fileDir = 'fotos'

    def save(self, *args, **kwargs):

        #obteniendo mes and year por motivos de filtros
        self.mes = self.fecha.month
        self.year = self.fecha.year
        super(Actividad, self).save(*args, **kwargs)

    def __unicode__(self):
        return u'%s - %s' % (self.nombre_actividad, self.fecha)

    def get_video_id(self):
        if self.video:
            url_data = urlparse.urlparse(self.video)
            query = urlparse.parse_qs(url_data.query)
            return query["v"][0]
        return None

    def get_vthumb(self):
        id = self.get_video_id()
        if id:
            return '<img width="128" height="96" src="http://img.youtube.com/vi/%s/2.jpg" alt="thumb">' % id
        return ''

    def get_video(self):
        id = self.get_video_id()
        if id:
            return """http://www.youtube.com/embed/%s??showsearch=0&showinfo=0&iv_load_policy=3&autoplay=1""" % id
        return ''

    def clean(self):
        suma_base = self.hombres + self.mujeres
        suma_edad = self.adultos + self.jovenes + self.ninos
        suma_tipo = self.autoridades + self.maestros + self.lideres + self.pobladores + self.estudiantes + self.miembros + self.tecnicos

        if not self.no_dato:
            if suma_base != suma_edad:
                raise ValidationError('La suma de los participantes por edad no concuerda')

        if not self.no_dato1:
            if suma_base != suma_tipo:
                raise ValidationError('La suma de los participantes por tipo no concuerda')

    class Meta:
        verbose_name_plural = u'Actividades'
