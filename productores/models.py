# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from lugar.models import Departamento, Municipio, Comunidad

CHOICE_SEXO = (
                (1, 'Mujer'),
                (2, 'Hombre'),
              )

CHOICE_JEFE = (
                (1, 'Si'),
                (2, 'No'),
              )

@python_2_unicode_compatible
class Personas(models.Model):
    nombre = models.CharField(max_length=250)
    cedula = models.CharField(max_length=50, null=True, blank=True)
    sexo = models.IntegerField(choices=CHOICE_SEXO)
    jefe = models.IntegerField(choices=CHOICE_JEFE)
    fecha_nacimiento = models.DateField('Fecha de nacimiento')
    departamento = models.ForeignKey(Departamento)
    municipio = models.ForeignKey(Municipio)
    comunidad = models.ForeignKey(Comunidad)
    finca = models.CharField(max_length=250)
    es_promotor = models.BooleanField()
    pertenece_grupo_asociativo = models.BooleanField()
    latitud = models.FloatField()
    longitud = models.FloatField()

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        ordering = ('nombre', )

    def __str__(self):
        return u'%s' % (self.nombre)

@python_2_unicode_compatible
class Recolectores(models.Model):
    nombre = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        self.nombre = self.nombre.upper()
        super(Recolectores, self).save(*args, **kwargs)

    class Meta:
        unique_together = ('nombre',)
        verbose_name_plural = 'Recolectores'
        ordering = ('nombre', )

    def __str__(self):
        return self.nombre
