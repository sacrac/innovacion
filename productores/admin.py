from django.contrib import admin
from .models import *

class PersonaAdmin(admin.ModelAdmin):
    list_display = ('id','nombre','es_promotor','departamento','fecha_nacimiento')
    list_filter = ('es_promotor','pertenece_grupo_asociativo',)
    search_fields = ('nombre',)

# Register your models here.
admin.site.register(Personas, PersonaAdmin)
admin.site.register(Recolectores)
#admin.site.register(Personas)
