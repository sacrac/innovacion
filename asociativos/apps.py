from __future__ import unicode_literals

from django.apps import AppConfig


class AsociativosConfig(AppConfig):
    name = 'asociativos'
    verbose_name = 'Grupos asociativos'
