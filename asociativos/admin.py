from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Rubros)
admin.site.register(Semilla)
admin.site.register(MateriaProcesada)
admin.site.register(Certificacion)
admin.site.register(AreaTrabajo)
admin.site.register(Asociaciones)
