# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.contrib.auth.models import User
from productores.models import Personas
from lugar.models import Departamento, Municipio, Comunidad

@python_2_unicode_compatible
class SelectorBase(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre

    class Meta:
        abstract = True
        ordering = ['nombre']

class Rubros(SelectorBase):
    pass

class Semilla(SelectorBase):
    pass

class MateriaProcesada(SelectorBase):
    pass

class Certificacion(SelectorBase):
    pass

class AreaTrabajo(SelectorBase):
    pass

@python_2_unicode_compatible
class Asociaciones(models.Model):
    nombre = models.CharField('Nombre', max_length=150)
    fecha_est = models.DateField('fecha de establecimiento', blank=True, null=True)
    direccion = models.TextField(blank=True, null=True)
    municipio = models.ForeignKey(Municipio)
    lat = models.DecimalField(blank=True, max_digits=8,
            decimal_places=2, verbose_name='latitud', null=True)
    lon = models.DecimalField(blank=True, max_digits=8,
            decimal_places=2, verbose_name='longitud', null=True)
    telefono = models.CharField(max_length=8, blank=True, null=True)
    celular = models.CharField(max_length=8, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    pagina_web = models.URLField(blank=True, null=True)
    representante_legal = models.CharField(max_length=100, blank=True, null=True)
    representante_tecnico = models.CharField(max_length=100, blank=True, null=True)
    num_hombres = models.IntegerField('numero de miembros hombres', blank=True, null=True)
    num_mujeres = models.IntegerField('numero de miembros mujeres', blank=True, null=True)

    # Campos de control y seguridad
    user = models.ForeignKey(User) #usuario que creó el registro
    fecha_agregado = models.DateTimeField(auto_now_add=True)
    fecha_actualizado = models.DateTimeField(auto_now=True)

    # Rubros comunes
    rubros = models.ManyToManyField(Rubros, blank=True)
    semillas = models.ManyToManyField(Semilla, blank=True)
    materia_procesada = models.ManyToManyField(MateriaProcesada, blank=True)
    certificacion = models.ManyToManyField(Certificacion, blank=True)
    area_trabajo = models.ManyToManyField(AreaTrabajo, blank=True)
    miembros = models.ManyToManyField(Personas, blank=True, verbose_name='Miembros del grupo asociativo')

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Grupo asociativo'
        verbose_name_plural = 'Grupos asociativos'
        ordering = ['nombre']
