# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from productores.models import Personas
from django.contrib.auth.models import User
from lugar.models import Departamento, Municipio
from smart_selects.db_fields import ChainedForeignKey
from sorl.thumbnail import ImageField

from django.db import models

CHOICE_EDUCACION = (
            (1, 'Analfabeta'),
            (2, 'Alfabetizado'),
            (3, 'Primaria incompleta'),
            (4, 'Primaria completa'),
            (5, 'Secundaria no completa'),
            (6, 'Secundaria completa'),
            (7, 'Técnico/a'),
            (8, 'Universitario/a'),
        )

@python_2_unicode_compatible
class OrganizacionCampesina(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Organizaciones campesinas"

@python_2_unicode_compatible
class OrganizacionCivil(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Organizaciones civiles"

@python_2_unicode_compatible
class EscuelaCampo(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Escuelas de campos"

@python_2_unicode_compatible
class TipoSuelo(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Tipos de suelos"

@python_2_unicode_compatible
class CultivosFinca(models.Model):
    nombre = models.CharField(max_length=200)
    image_cultivos = models.ImageField(upload_to='cultivos/', blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Cultivos en la finca"

@python_2_unicode_compatible
class AnimalesFinca(models.Model):
    nombre = models.CharField(max_length=200)
    image_animales = models.ImageField(upload_to='animales/', blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Animales en la finca"

@python_2_unicode_compatible
class ProductoProcesado(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Productos procesados"

@python_2_unicode_compatible
class MercadoAcceso(models.Model):
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Mercados de accesos"

@python_2_unicode_compatible
class Promotor(models.Model):
    persona = models.ForeignKey(Personas)
    innovador = models.BooleanField()
    educacion = models.IntegerField('Nivel de educación formal',
                                    choices=CHOICE_EDUCACION)
    contacto = models.CharField(help_text='Número de celular', max_length=50,
                                null=True, blank=True)
    activo = models.CharField('Años activos en prueba de practicas', max_length=250)
    departamento = models.ForeignKey(Departamento)
    municipio = ChainedForeignKey(
        Municipio,
        chained_field="departamento",
        chained_model_field="departamento",
        show_all=False,
        auto_choose=True
    )
    organizacion_campesina = models.ForeignKey(OrganizacionCampesina,
                             verbose_name=u'Nombre de organización campesina que pertenece')
    organizacion_civil = models.ForeignKey(OrganizacionCivil,
                         verbose_name=u'Nombre Organización de sociedad civil que lo apoya')
    escuela = models.ForeignKey(EscuelaCampo, verbose_name=u'Escuela promotores que asiste')
    tipo_suelo = models.ForeignKey(TipoSuelo)
    tipo_clima = models.IntegerField(choices=((1, 'Árido'),(2, 'Seco'),
                                              (3, 'Semi-seco'),(4, 'Semi-húmedo'),
                                              (5, 'Húmedo'),))
    meses_lluvia = models.IntegerField()
    finca = models.FloatField('Tamaño de finca en Mz')
    riego = models.IntegerField(choices=((1, 'No hay'),(2, 'Por gravedad'),
                                         (3, 'Por aspersión'),(4, 'Por goteo'),))
    bosque = models.FloatField()
    potrero = models.FloatField('Potrero con árboles')
    tacotales = models.FloatField()
    forestal = models.FloatField('Plantación forestal')
    perennes = models.FloatField('Cultivo Perennes')
    lena = models.FloatField('Plantación para leñas')
    anuales = models.FloatField('Cultivo Anuales')
    frutales = models.FloatField()
    potrero_abierto = models.FloatField('Potrero abierto')
    patio = models.FloatField()
    cultivos_finca = models.ManyToManyField(CultivosFinca)
    animales_finca = models.ManyToManyField(AnimalesFinca)
    producto_procesado = models.ManyToManyField(ProductoProcesado)
    mercado_accede = models.ManyToManyField(MercadoAcceso)

    usuario = models.ForeignKey(User, null=True, blank=True)

    class Meta:
        verbose_name = "Promotor"
        verbose_name_plural = "Promotores"
        ordering = ('persona',)

    def __str__(self):
        return u'%s' % (self.persona)
