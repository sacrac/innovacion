from django.contrib import admin
from .models import *

class PromotorAdmin(admin.ModelAdmin):
    list_display= ['persona','innovador','educacion','finca']
    search_fields = ('persona',)
    list_filter = ['escuela',]
    filter_horizontal = ('cultivos_finca','animales_finca',
                        'producto_procesado','mercado_accede')
    fieldsets = (
        (None, {
            'fields': (('persona', 'innovador', ), ('educacion','contacto','activo'),
                      ( 'departamento', 'municipio'),('organizacion_campesina',
                      'organizacion_civil','escuela','tipo_suelo','tipo_clima'),
                      ('meses_lluvia','finca','riego'),)
        }),
        ('Uso de tierra', {
            'fields': (('bosque', 'potrero'),('tacotales','forestal'),('perennes','lena'),
                        ('anuales','frutales'),('potrero_abierto','patio'),)
        }),
        (None, {
            'fields': (('cultivos_finca', 'animales_finca'),
                        ('producto_procesado','mercado_accede'),)
        }),

    )

admin.site.register(Promotor, PromotorAdmin)
admin.site.register(OrganizacionCampesina)
admin.site.register(OrganizacionCivil)
admin.site.register(EscuelaCampo)
admin.site.register(TipoSuelo)
admin.site.register(CultivosFinca)
admin.site.register(AnimalesFinca)
admin.site.register(ProductoProcesado)
admin.site.register(MercadoAcceso)
